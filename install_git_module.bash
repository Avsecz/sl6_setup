#!/bin/bash
#
# install emacs-$VERSION to module system
#
# author: avsec
############################################

# modifiable variables

VERSION=2.7.0

program=git
############################################
## stop on error:
set -e 

# required paths
program_dir=/gcm/opt/gagneur/$program

install_dir=${program_dir}/$VERSION

module_file=/gcm/opt/modules/modulefiles/gagneur/$program/$VERSION

###################################################################################################
# installation procedure, change here

# install required dependencies 

sudo yum -y install curl-devel expat-devel gettext-devel openssl-devel zlib-devel gcc perl-ExtUtils-MakeMaker

# download an unpack tarball to ${emacs_dir}/src
mkdir -p ${program_dir}/src
cd ${program_dir}/src

wget https://www.kernel.org/pub/software/scm/git/git-$VERSION.tar.gz
tar xzvf git-$VERSION.tar.gz

# build and install from source to ${install_dir}
cd git-$VERSION
make prefix=${install_dir} all
make prefix=${install_dir} install

###################################################################################################

### add the software to the modulesystem by creating the module file
mkdir -p /gcm/opt/modules/modulefiles/gagneur/$program

echo '#%Module1.0
## 
## author: ziga avsec
##
proc ModulesHelp { } {
        global version modroot

        puts stderr "\tmodules - loads the modules software & application environment"
        puts stderr "\n\tThis adds $modroot/* to several of the"
        puts stderr "\tenvironment variables."
        puts stderr "\n\tVersion $version\n"
}

module-whatis   "Adds '"${program}"' to PATH"

'"prepend-path PATH  ${install_dir}/bin/" > ${module_file}


echo "Installation finished"
