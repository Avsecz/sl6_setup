## Prepare your SL6 at work

- install packages listed in packages-list.txt
- install additional software with install_* (listed in install_additional.bash)
- configure shortcuts for
    - bash
    - zsh
    - git
    - svn
    - emacs
    - terminator

### Usage:

run:
`./install.bash`
