## this script will setup the whole configuration

# TODO
# - auto setup file browser view ?


# setup droak
cd ~
## setup te proper rights
# chmod -R u=rwX,go= SpiderOak\ Hive/
ln -s ~/Dropbox/droak ~/droak

CONFIG=$HOME/droak/config

## terminator
mkdir .config/terminator
ln -s $CONFIG/terminator/config-qbm ~/.config/terminator/config

## bashrc
mv .bashrc .bashrc.bak
ln -s $CONFIG/bash/dot-bashrc-qbm.sh ~/.bashrc
ln -s $CONFIG/bash/dot-bashrc-h.d ~/.bashrc.d

## zsh
ln -s $CONFIG/zsh/dot-zshrc ~/.zshrc
ln -s $CONFIG/zsh/dot-oh-my-zsh ~/.oh-my-zsh
## set zsh as a default bash
chsh -s $(which zsh)

## git
ln -s $CONFIG/git/dot-gitconfig ~/.gitconfig
ln -s $CONFIG/git/dot-gitignore ~/.gitignore

## svn 
mv ~/.subversion/config ~/.subversion/config.bak
ln -s $CONFIG/subversion/config ~/.subversion/config

############################################
# emacs
## get .emacs, .emacs.d
$CONFIG/emacs/dotemacs/install-emacs-config.sh
## add emacs to the launcher:
cp $CONFIG/compiz/emacsclient.desktop ~/.local/share/applications/
## add emacs to the launcher:
cp $CONFIG/compiz/emacsclient_open.desktop ~/.local/share/applications/


# old 
# mv ~/.emacs.d ~/.emacs.d.bak
# ln -s $CONFIG/emacs/dotemacs.el ~/.emacs
# ln -s $CONFIG/emacs/emacs.d ~/.emacs.d
############################################

## link /home/avsecz -> ~/
# sudo ln -s $HOME /home/avsecz

## ssh
# mkdir ~/.ssh
# ln -s $CONFIG/ssh/config ~/.ssh/config

## rebind caps lock to Ctrl

# sh -c "echo '\nXKBOPTIONS=\"ctrl:nocaps\"\n' >> /etc/default/keyboard"

# gnome config
# https://wiki.archlinux.org/index.php/GNOME_Files
