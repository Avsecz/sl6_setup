#!/bin/bash
## stop on error:
set -e 
############################################

# prepare your SL6 at work
# 
# - install packages listed in packages-list.txt
# - install additional software with install_* (listed in install_additional.bash)
# - configure shortcuts for
#   - bash
#   - zsh
#   - git
#   - svn
#   - emacs
#   - terminator

############################################
# install main packages
############################################

sudo yum -y update
sudo yum -y upgrade

## test 
yum deplist $(grep -vE "^\s*#" packages-list.txt  | tr "\n" " ")
## run
sudo yum -y install $(grep -vE "^\s*#" packages-list.txt  | tr "\n" " ")

sudo yum -y update
sudo yum -y upgrade

############################################
# install additional software
############################################

# get the additional install functions
source ./install_additional.bash

# install_emacs
install_aspell
install_vbox
install_dropbox

############################################
# configuration
############################################

# conf_mouse
# conf_arrow_up_history
# conf_guake
# conf_caps_to_ctrl


# configure the links (after loading Dropbox)
# ./configuration-setup.sh
