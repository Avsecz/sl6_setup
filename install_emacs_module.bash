#!/bin/bash
#
# install emacs-$VERSION to module system
#
# author: avsec
############################################

# modifiable variables

VERSION=24.5

############################################
## stop on error:
set -e 

# required paths
emacs_dir=/gcm/opt/gagneur/emacs

install_dir=${emacs_dir}/$VERSION

module_file=/gcm/opt/modules/modulefiles/gagneur/emacs/$VERSION

# install required dependencies 

sudo yum-builddep -y emacs

# download an unpack tarball to ${emacs_dir}/src
mkdir -p ${emacs_dir}/src
cd ${emacs_dir}/src

wget http://ftp.gnu.org/pub/gnu/emacs/emacs-$VERSION.tar.gz
tar xzvf emacs-$VERSION.tar.gz

# build and install from source to ${install_dir}
cd emacs-$VERSION
./configure --prefix=${install_dir}
make
make install

### add the software to the modulesystem by creating the module file
mkdir -p /gcm/opt/modules/modulefiles/gagneur/emacs

echo '#%Module1.0
## 
## author: ziga avsec
##
proc ModulesHelp { } {
        global version modroot

        puts stderr "\tmodules - loads the modules software & application environment"
        puts stderr "\n\tThis adds $modroot/* to several of the"
        puts stderr "\tenvironment variables."
        puts stderr "\n\tVersion $version\n"
}

module-whatis   "Adds emacs to PATH"

'"prepend-path PATH  ${install_dir}/bin/" > ${module_file}


echo "Installation finished"
