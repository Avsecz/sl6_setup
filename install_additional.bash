#!/bin/bash

# dropbox
install_dropbox () {
    cd ~
    mkdir -p tmp
    # wget https://www.dropbox.com/download?dl=packages/fedora/nautilus-dropbox-2015.10.28-1.fedora.x86_64.rpm
    # sudo rpm -i nautilus-dropbox-2015.10.28-1.fedora.x86_64.rpm
    cd tmp && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
    
    $HOME/.dropbox-dist/dropboxd & 
    sudo yum -y install nautilus-dropbox.x86_64

    # start dropbox automatically
# https://gist.github.com/briangonzalez/6903025
    # chkconfig dropbox on
}

# emacs + dependencies
install_emacs () {
    version=24.5

    # install required dependencies 
 
    sudo yum-builddep emacs
    # sudo yum install  gcc make ncurses-devel giflib-devel libjpeg-devel libtiff-devel gtk2-devel libXpm-devel.i386 giflib-devel.i386 libtiff-devel.i386 libjpeg-devel.i386 libgtk-3-dev
    # libtinfo libncurses libterminfo libtermcap libcurses


    # download an unpack tarball
    cd ~
    mkdir -p tmp
    cd tmp
    wget http://ftp.gnu.org/pub/gnu/emacs/emacs-$version.tar.gz
    tar xzvf emacs-$version.tar.gz

    # build from source
    cd emacs-$version
    ./configure 
    make
    sudo make install
    
    # check the installation
    echo "Installation finished"
    echo "Running: which emacs"
    which emacs
    echo "Running: emacs --version"
    emacs --version
}

#  TODO - finish
#  install aspell+ add aspell to ~/.bin
install_aspell () {
    echo "Install aspell"
    sudo yum -y install aspell aspell-en
    # 1. sudo yum install aspell aspell-en
    # 2. downloadaj iz ftp://ftp.gnu.org/gnu/aspell/dict/
    # de in sl
    # *.tar.bz2 fajl.
    # tar -xvf aspell*.tar.bz2
    # potem pa narediš
    # ./configure
    # make
    # sudo make install
    # premakni folder v ~/BIN
}

install_vbox () {
    # taken from:
    # http://www.if-not-true-then-false.com/2010/install-virtualbox-with-yum-on-fedora-centos-red-hat-rhel/

    # get vbox repo:
    wget http://download.virtualbox.org/virtualbox/rpm/rhel/virtualbox.repo

    # add epel 
    rpm -Uvh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

    # packages to install
    sudo yum -y install binutils gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel dkms

    # install 
    sudo yum -y install VirtualBox-5.0

    sudo /etc/init.d/vboxdrv setup

    # usernames

    # TODO 
    # ter ustvaril mapo na
    # /data/vbox
    # z pravicami
    # chmod 1777 /data/vbox/

    echo "virtualbox installed"

    echo 'Things you might do:
usermod -a -G vboxusers avsec
sudo mkdir -p /data/vbox
sudo chmod 1777 /data/vbox/

Set in preferences (run VirtualBox ):


default machine folder: /data/vbox

and

system/processor:
Enable PAE/NX Checked
'
}


# Rebind back and forward keys to Alt-Left and Alt-Right
conf_mouse () {
    sudo yum -y install xbinkeys xvkbd
    echo '
"/usr/bin/xvkbd -text "\[Alt_L]\[Left]""
m:0x0 + b:8
"/usr/bin/xvkbd -text "\[Alt_L]\[Right]""
m:0x0 + b:9
' >> ~/.xbindkeysrc

}

conf_arrow_up_history () {
    echo '
## arrow up
"\e[1;5A":history-search-backward
## arrow down
"\e[1;5B":history-search-forward
' >> .inputrc
}

# add guake to startup
conf_guake () {
    cp /usr/share/applications/guake.desktop /etc/xdg/autostart/
}

conf_caps_to_ctrl () {
    # sh -c "echo '\nXKBOPTIONS=\"ctrl:nocaps\"\n' >> /etc/default/keyboard"
    setxkbmap -option ctrl:nocaps
}
